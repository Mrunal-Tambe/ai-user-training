# DEEP NEURAL NETWORK(DNN) SUMMARY
## Neurons of Input
- **Activation values** - Neurons contain corresponding grayscale values of the pixels (from 0 to 1).
- 1st layer of DNN.
## Output Neurons
- For Digit recognition, output neurons represnt each digit, hence, the number is 10.
- The values in these neurons corresponds to the probability that the classifier thinks that a particular image contains a particular digit.
## Hidden Neuron Layers
- There can be multiple layers.
- Activation of neurons in a layer is dependent on activation of neurons in previous layers.
- Each layer corresponds to a certain feature of the image and depending on the presence of that feature specific neurons are activated.
## Activation of Neurons
- Each neuron of a layer is assigned with a weight. Weighted sum of all the neurons in the layer is calculated and depending on the result the neuron in next layer is activated.
-  Activation of neurons is not binary.
### Sigmoid curve
- The weighted sum calculated above need to be compressed to values within 0 to 1. This is done using sigmoid curve. Negative weight converge towards 0 and highly positive weights converge to 1.
- Matrix multiplication of vector of activation values of neurons and the matrix containing their weights and add bias matrix to it.
**Not suitable for training purposes.**

![Sigmoid Curve](/uploads/64f39d62af8b33bc520099cd34135ce0/CNN_flow.png)

### Bias
A negative integer is added to the weighted sum, when neuron need to be activated only when the sum is greater than a certain value called *Bias*.
### Changing bias and weights values determines the activated of neurons and the behaviour of the network.

### Rectified Linear Unit (ReLU)
- It is used instead of Sigmoid function.
- If the value is less than the certain threshold, value of ReLU = 0. If value is greater, then ReLU function is identity function **f(a) = a**.
![CNN_flow](/uploads/1cbfdb127286c62d5ada44b5d7af24bc/CNN_flow.png)
## Training Algorithms
- Require dataset with labels which seperated as training and testing dataset.
- Evaluate the algorithm for accuracy.
### Cost function
- For a particular output, ideally only 1 neuron should 1 and others should be 0.
- Practically to evaluate how efficient the network is calculate the square of difference between the ideal values (0 or 1) and obtained values. The lower the cost, more efficient is the network.
### Minimising the cost function
- Trivial way - Calculate the derivative and equate it to 0. But its not suitable for complicated functions.
- Calculate the slope and then: Shift right - If slope is positive
                                Shift left - If slope is negative
- Step size should be variable according to the slope.
-  For 2 variables: Calculate the vector in direction of maximum ascent. Opposite direction of this vector indicates maximum descent. This should be the direction to move for reaching the minima. Reducing the average cost function improves the performance of the entire network.
**Obtaining local minima is doable, global minima in hard to find.**

## Backtracking Algorithm
The algorithm for computing the gradient efficiently which is crucial for learning phase of the network.
- **Gradient Vector** - Elements of the vector determine how they impact the network. Negative sign indicates inhibitory relation. Magnitude indicates relative importance of each weight and bias.

## Flattening of multi-dimensional Input
- To activate the neurons in **Fully Connected Layer** of CNN, **tensors** (A whole batch of images) need to be flattened. They are converted into a 1 dimensional vector.
- While flattening, image tensors are flattened but the batch tensors are retained.
### Architecture of Tensors 
![Tensor Architecture](/uploads/eabec205ab361e2b9646d3696abe6454/CNN_flow.png)



#### Extras Info
1. Convolutional Neural Network - For Image recognition
2. Long-Short Term Memory Network - For Speech recognition
