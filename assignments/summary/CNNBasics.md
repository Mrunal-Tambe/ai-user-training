# CONVOLUTIONAL NEURAL NETWORK (CNN)
## What is a NEURAL NETWORK?
- It is a network of artificial nodes analogous to biological neural network. It has 3 layers - 1. Input 2. Hidden 3. Output

[Neural Network Layers](https://en.wikipedia.org/wiki/Neural_network#/media/File:Neural_network_example.svg)

- Hidden layers are used to extract various feautures from the input and classify it.

## What is CONVOLUTIONAL NEURAL NETWORK?
- It is a class of deep neural networks and is used for image analysis.
[CNN Block Diagram](https://gitlab.com/iotiotdotin/ai-user-training/-/wikis/uploads/702fa72cb430640071b6c21e80dfbbfb/dnn1.png)

- **Terms used in CNN**:
1. **Image** - It is the input matrix to the CNN from which features are matched with the filter.
2. **Filter** - It is a matrix containing values corresponding to a particular feature.
3. **Kernel Size** - Size of sliding window in pixels. *Smaller and odd size is prefered.*
4. **Stride** - Number of pixels filter will slide at each convolution.
5. **Zero padding** - Amount of zeroes added to image border to ensure convolution of egde pixels of image.
6. **Flattening** - Coverts feature matrices to a vector.
7. **Activation Function** - Decides which neurons to activate. **Commonly used - ReLu**
8. **Output Layer** - Classifier of images. **Commonly used - Softmax**

## Flow of CNN
![CNN_flow](/uploads/bbab7c536255f3de6c2496dabdc68ea9/CNN_flow.png)
